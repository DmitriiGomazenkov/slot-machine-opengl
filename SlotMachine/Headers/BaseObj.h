#pragma once
#include "glm/vec2.hpp"

// ������� ����� ������� �����
class BaseObj
{
public:
	BaseObj(glm::vec2 pos);
	virtual ~BaseObj();
	virtual void Render() = 0;

public:
	glm::vec2 m_Pos;
};