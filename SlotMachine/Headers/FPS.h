#pragma once
#include "UpdatableObj.h"
#include <string>

class FPS : public UpdatableObj
{
public:
	FPS(glm::vec2 pos);
	virtual void Render() override;
	virtual void Update(int deltaTime) override;

private:
	std::string m_FpsTxt;
	std::string m_FullTxt;
	unsigned int m_FrameCounter;
	int m_Time;
	int m_Timebase;
};