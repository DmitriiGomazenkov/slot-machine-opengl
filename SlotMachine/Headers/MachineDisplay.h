#pragma once
#include "Wheel.h"
#include <memory>

class StartBtn;

class MachineDisplay : public UpdatableObj
{
public:
	MachineDisplay(glm::vec2 pos);
	virtual void Render() override;
	virtual void Update(int deltaTime) override;

	void StartRoll(int rollTime);
	void WheelStoped();
	void SubscribeBtn(std::weak_ptr<StartBtn> startBtn);

private:
	std::list<Wheel> m_Wheels;
	std::shared_ptr<SpriteObj> m_ForegrndUp;
	std::shared_ptr<SpriteObj> m_ForegrndBottom;
	std::shared_ptr<SpriteObj> m_Backgrnd;

	std::weak_ptr<StartBtn> m_StartBtn;
	unsigned int m_StopCounter;
};