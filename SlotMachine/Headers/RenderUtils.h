#pragma once
#include "glm/vec2.hpp"
#include <string>

void RenderSprite(const std::string &spriteName, float posX, float posY, float scale);
void RenderSprite(const std::string &spriteName, float posX, float posY, float scaleX, float scaleY);

// ����� ��� ������� ����
void RenderTxt(const char *str, glm::vec2 pos);

// ����� � �������� ���� (�������)
void RenderTxt(const char *str, glm::vec2 pos, float scale, glm::vec2 backgrndColor, bool showBack = true);

// ������������� ������ �����������
void init2D(float r, float g, float b);

// ������ � ����� ���������
#define START_RENDER glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
#define END_RENDER glFlush();