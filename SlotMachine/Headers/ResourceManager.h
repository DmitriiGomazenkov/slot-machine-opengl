#pragma once
#include <map>
#include <string>
#include "Texture2D.h"

class ResMngrSinglton
{
public:
	static ResMngrSinglton& GetInstance();

	ResMngrSinglton(ResMngrSinglton const&) = delete;
	void operator=(ResMngrSinglton const&) = delete;
	~ResMngrSinglton();

	Texture2D LoadTexture(const char *file, GLboolean alpha, std::string name);
	Texture2D GetTexture(const std::string &name);

private:
	ResMngrSinglton() { }
	Texture2D loadTextureFromFile(const char *file, GLboolean alpha);
	void Clear();

private:
	std::map<std::string, Texture2D> m_Textures;
};