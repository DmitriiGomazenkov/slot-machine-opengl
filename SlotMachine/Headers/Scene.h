#pragma once
#include "UpdatableObj.h"
#include "StartBtn.h"
#include "FPS.h"
#include <vector>
#include <memory>
#include "glm/vec2.hpp"

class MachineDisplay;

class SceneSingleton
{
public:
	static SceneSingleton& GetInstance();
	SceneSingleton(SceneSingleton const&) = delete;
	void operator=(SceneSingleton const&) = delete;

	void Init();
	void MousBtnPressed(int button, int state, int x, int y);

	void Render();
	void Update();

private:
	SceneSingleton();

private:
	std::vector<std::shared_ptr<UpdatableObj>> m_SceneObjs;
	std::shared_ptr<MachineDisplay> m_MachineDisplay;
	std::shared_ptr<FPS> m_FPS;
	std::shared_ptr<StartBtn> m_StartBtn;
	int m_LastTime;
};