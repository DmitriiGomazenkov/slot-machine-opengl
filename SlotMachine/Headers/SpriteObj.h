#pragma once
#include "BaseObj.h"
#include <string>

class SpriteObj : public BaseObj
{
public:
	SpriteObj(glm::vec2 pos, float scale, const std::string &sprite);
	SpriteObj(glm::vec2 pos, float scaleX, float scaleY, const std::string &sprite);
	virtual void Render() override;

private:
	const std::string m_Srites;
	const float m_ScaleX;
	const float m_ScaleY;
};