#pragma once
#include "UpdatableObj.h"
#include "MachineDisplay.h"
#include <string>

class StartBtn : public UpdatableObj
{
public:
	StartBtn(glm::vec2 pos, std::weak_ptr<MachineDisplay> machineDisplay);
	virtual void Render() override;
	virtual void Update(int deltaTime) override;

	// ���� ���������� ����� ������� � 
	// ������������ ������, �� ������������ ����
	void TestAndClick(int x, int y);
	void DisableBtn(bool disable = true);

private:
	std::string m_BtnName;
	glm::vec2 m_BackColor;
	float m_Time;
	float m_Scale;
	bool m_IsClicked;

	// ����������� ������
	std::weak_ptr<MachineDisplay> m_MachineDisplay;
};