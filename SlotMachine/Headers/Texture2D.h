#pragma once

class Texture2D
{
public:
	GLuint m_ID;
	GLuint m_Width, m_Height;
	GLuint m_InternalFormat;
	GLuint m_ImageFormat;

	GLuint m_WrapS;
	GLuint m_WrapT;
	GLuint m_FilterMin;
	GLuint m_FilterMax;

	Texture2D();
	void Generate(GLuint width, GLuint height, unsigned char* data);
	void Bind() const;
};