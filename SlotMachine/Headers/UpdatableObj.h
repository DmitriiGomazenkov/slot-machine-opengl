#pragma once
#include "BaseObj.h"

class UpdatableObj : public BaseObj
{
public:
	UpdatableObj(glm::vec2 pos);
	virtual ~UpdatableObj();
	virtual void Update(int deltaTime) = 0;
};