#pragma once
#include "UpdatableObj.h"
#include "SpriteObj.h"
#include <list>

class MachineDisplay;

class Wheel : public UpdatableObj
{
public:
	Wheel(glm::vec2 pos, MachineDisplay *machineDisplay);
	virtual void Render() override;
	virtual void Update(int deltaTime) override;

	void StartRoll(float speed, int rollTime);

private:
	class RollTimeMngr
	{
	public:
		RollTimeMngr();

		// ��������������� � �������� �������
		bool IsStopTime(int time);
		void SetTargetTime(int time);

	public:
		bool m_RollStarted;

	private:
		int m_CurrRollTime;
		int m_TargetRollTime;
	};

private:
	std::list<SpriteObj> m_Slots;
	float m_Speed;
	RollTimeMngr m_RollTimeMngr;

	float m_FirstInvisSlot;
	float m_CurYPos;

	MachineDisplay *m_MachineDisplay;
};