#include "FPS.h"
#include "glut.h"
#include "RenderUtils.h"
#include "Globals.h"


FPS::FPS(glm::vec2 pos)
	: UpdatableObj(pos)
	, m_FrameCounter(0)
	, m_Time(0)
	, m_Timebase(0)
	, m_FpsTxt("FPS: ")
{
}

void FPS::Render()
{
	RenderTxt(m_FullTxt.c_str(), m_Pos);
}

void FPS::Update(int deltaTime)
{
	++m_FrameCounter;
	m_Time += deltaTime;

	if (m_Time - m_Timebase >= ONE_SEC)
	{
		auto fps = std::to_string(m_FrameCounter * ONE_SEC / (m_Time - m_Timebase));
		m_FullTxt = m_FpsTxt + fps;

		m_Timebase = m_Time;
		m_FrameCounter = 0;
	}
}