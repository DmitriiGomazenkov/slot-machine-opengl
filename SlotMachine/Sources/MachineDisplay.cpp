#include <random>
#include "MachineDisplay.h"
#include "RenderUtils.h"
#include "StartBtn.h"
#include "Globals.h"

#define WHEELS_NUM 5

MachineDisplay::MachineDisplay(glm::vec2 pos) : UpdatableObj(pos)
{
	// ����������� �������� ������������ ��������� �������
	glm::vec2 curPos = m_Pos + glm::vec2(-0.4f, 0.5f);

	auto AddWheel = [&]()
	{
		m_Wheels.push_back(Wheel(curPos, this));
		curPos.x += 0.2f;
	};

	for (unsigned int i = 0; i < WHEELS_NUM; ++i)
		AddWheel();

	// ����������� ������
	curPos = m_Pos + glm::vec2(0.0f, 0.1f);
	m_Backgrnd = std::make_shared<SpriteObj>(curPos, 0.5f, 0.3, "background");

	curPos = m_Pos + glm::vec2(0.0f, 0.5f);
	m_ForegrndUp = std::make_shared<SpriteObj>(curPos, 0.5f, 0.1f, "foreGround");

	curPos = m_Pos + glm::vec2(0.0f, -0.3f);
	m_ForegrndBottom = std::make_shared<SpriteObj>(curPos, 0.5f, 0.1f, "foreGround");
}

void MachineDisplay::Render()
{
	m_Backgrnd->Render();

	for (auto& wheel : m_Wheels)
		wheel.Render();

	m_ForegrndUp->Render();
	m_ForegrndBottom->Render();
}

void MachineDisplay::Update(int deltaTime)
{
	for (auto& wheel : m_Wheels)
		wheel.Update(deltaTime);
}

void MachineDisplay::StartRoll(int rollTime)
{
	// ���������� ������� ������������� ���������
	m_StopCounter = 0;

	auto Rand = [](float randMax) -> float
	{
		std::random_device rd;
		std::mt19937 eng(rd());

		int randMin = 1;

		std::uniform_int_distribution<> rand(randMin, randMax);
		return rand(eng);
	};

	// ������ ������ ����������� ����� 
	// ��������� �����, �� ���� �� �����
	auto time = rollTime;

	for (auto& wheel : m_Wheels)
	{
		float randMax = 10;

		time += Rand(randMax) / randMax * ONE_SEC;
		auto speed = Rand(randMax) / (randMax * randMax);

		wheel.StartRoll(speed, time);
	}
}

void MachineDisplay::WheelStoped()
{
	// ���� ��� �������� ������������
	if (++m_StopCounter >= WHEELS_NUM)
		m_StartBtn.lock()->DisableBtn(false);
}

void MachineDisplay::SubscribeBtn(std::weak_ptr<StartBtn> startBtn)
{
	m_StartBtn = startBtn;
}