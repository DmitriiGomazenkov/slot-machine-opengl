#include "glut.h"
#include "Scene.h"
#include "Globals.h"

float g_Width = 640.0f;
float g_Height = 640.0f;

#define FPS 60
#define UPDATE_TIME (ONE_SEC / FPS)

void MouseButton(int button, int state, int x, int y)
{
	SceneSingleton::GetInstance().MousBtnPressed(button, state, x, y);
}

void Idle(int)
{
	glutPostRedisplay();
	glutTimerFunc(UPDATE_TIME, Idle, 0);
}

void Display()
{
	SceneSingleton::GetInstance().Update();
	SceneSingleton::GetInstance().Render();
}

void Resize(int w, int h)
{
	if (!h)
		h = 1;

	g_Width = w;
	g_Height = h;
	glViewport(0, 0, w, h);
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	glutInitWindowPosition(0, 0);
	glutInitWindowSize(g_Width, g_Height);
	glutCreateWindow("Slot machine");

	SceneSingleton::GetInstance().Init();

	glutReshapeFunc(Resize);
	glutMouseFunc(MouseButton);
	glutDisplayFunc(Display);

	// FPS ����� ���� ������ 60, �.�. Idle() ����� ���������� ������ 16 ��, 
	// ������������� �� ���� ������� ����� ����� ���������� 63 ����
	glutTimerFunc(UPDATE_TIME, Idle, 0);

	glutMainLoop();
}