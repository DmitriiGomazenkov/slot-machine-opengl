#include "glut.h"
#include "RenderUtils.h"
#include "ResourceManager.h"
#include "Globals.h"

void RenderSprite(const std::string &spriteName, float posX, float posY, float scaleX, float scaleY)
{
	glEnable(GL_TEXTURE_2D);	// ���������� ��������� ��������
	glShadeModel(GL_SMOOTH);// ��������� ������� �������� �����������

	glMatrixMode(GL_PROJECTION);// ����� ������� ��������
	glPushMatrix();
	glLoadIdentity();		// ����� ������� ��������

	glMatrixMode(GL_MODELVIEW);// ����� ������� ��������� ������
	glPushMatrix();
	glLoadIdentity();

	float windowWidth(0);
	float windowHeight(0);
	auto mult = 1.0f;

	if (g_Width >= g_Height) 
	{
		windowWidth = mult * g_Width / g_Height;
		windowHeight = mult;
	}
	else 
	{
		windowWidth = mult;
		windowHeight = mult * g_Height / g_Width;
	}

	glOrtho(-1 * windowWidth, windowWidth, -1 * windowHeight, windowHeight, 1.0f, -1.0f);
	
	glTranslatef(posX, posY, 0.0f);
	glScalef(scaleX, scaleY, 0.0f);

	ResMngrSinglton::GetInstance().GetTexture(spriteName).Bind();
	glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f, -1.0f, 0.0f);	// ��� ����
		glTexCoord2f(1.0f, 1.0f); glVertex3f(1.0f, -1.0f, 0.0f);	// ��� �����
		glTexCoord2f(1.0f, 0.0f); glVertex3f(1.0f, 1.0f, 0.0f);	// ���� �����
		glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f, 1.0f, 0.0f);	// ���� ����
	glEnd();

	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);

	glDisable(GL_TEXTURE_2D);
}

void RenderSprite(const std::string &spriteName, float posX, float posY, float scale) 
{
	RenderSprite(spriteName, posX, posY, scale, scale);
}

void RenderTxt(const char *str, glm::vec2 pos)
{
	RenderTxt(str, pos, 0, glm::vec2(), false);
}

void RenderTxt(const char *str, glm::vec2 pos, float scale, glm::vec2 backgrndColor, bool showBack)
{
	glPushMatrix();
	glLoadIdentity();

	gluOrtho2D(0, g_Width, 0, g_Height);
	glRasterPos2i(pos.x - scale / 2, pos.y);

	// ���������� ������� ������� ���� ��� ���������
	if (showBack)
	{
		glColor3f(backgrndColor.x, backgrndColor.y, backgrndColor.y);
		glTranslatef(pos.x, pos.y, 0.0f);
		glScalef(scale, scale, 0.0f);

		glBegin(GL_QUADS);
		glVertex2f(-1.0f, -1.0f);	// ��� ����
		glVertex2f(1.0f, -1.0f);	// ��� �����
		glVertex2f(1.0f, 1.0f);		// ���� �����
		glVertex2f(-1.0f, 1.0f);	// ���� ����
		glEnd();
	}
	int len = strlen(str);

	glColor3f(1, 1, 1);
	for (int i = 0; i < len; ++i)
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, str[i]);

	glPopMatrix();
}

void init2D(float r, float g, float b)
{
	glClearColor(r, g, b, 0.0);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}