#include <windows.h>
#include <gl\gl.h>
#include "ResourceManager.h"
#include <SOIL.h>


ResMngrSinglton& ResMngrSinglton::GetInstance()
{
	static ResMngrSinglton instance;
	return instance;
}

ResMngrSinglton::~ResMngrSinglton()
{
	Clear();
}

Texture2D ResMngrSinglton::LoadTexture(const char *file, GLboolean alpha, std::string name)
{
	m_Textures[name] = loadTextureFromFile(file, alpha);
	return m_Textures[name];
}

Texture2D ResMngrSinglton::GetTexture(const std::string &name)
{
	return m_Textures[name];
}

void ResMngrSinglton::Clear()
{	
	for (auto iter : m_Textures)
		glDeleteTextures(1, &iter.second.m_ID);
}

Texture2D ResMngrSinglton::loadTextureFromFile(const char *file, GLboolean alpha)
{
	Texture2D texture;
	if (alpha)
	{
		texture.m_ImageFormat = GL_RGBA;
		texture.m_ImageFormat = GL_RGBA;
	}

	int width, height;
	unsigned char* image = SOIL_load_image(file, &width, &height, 0, texture.m_ImageFormat == GL_RGBA ? SOIL_LOAD_RGBA : SOIL_LOAD_RGB);

	texture.Generate(width, height, image);
	SOIL_free_image_data(image);
	return texture;
}