#include "Scene.h"
#include "MachineDisplay.h"
#include "glut.h"
#include "RenderUtils.h"
#include "ResourceManager.h"
#include "Globals.h"


SceneSingleton& SceneSingleton::GetInstance()
{
	static SceneSingleton instance;
	return instance;
}

void SceneSingleton::Init()
{
	init2D(0.4f, 0.5f, 0.5f);

	// �������� ���� ��������
	ResMngrSinglton::GetInstance().LoadTexture("textures\\Background.png", GL_TRUE, "background");
	ResMngrSinglton::GetInstance().LoadTexture("textures\\ForeGround.png", GL_TRUE, "foreGround");
	ResMngrSinglton::GetInstance().LoadTexture("textures\\Seven.png", GL_TRUE, "seven");
	ResMngrSinglton::GetInstance().LoadTexture("textures\\WaterM.png", GL_TRUE, "waterM");
	ResMngrSinglton::GetInstance().LoadTexture("textures\\Plum.png", GL_TRUE, "plum");
	ResMngrSinglton::GetInstance().LoadTexture("textures\\Lemon.png", GL_TRUE, "lemon");
	ResMngrSinglton::GetInstance().LoadTexture("textures\\Banana.png", GL_TRUE, "banana");
	ResMngrSinglton::GetInstance().LoadTexture("textures\\BigWin.png", GL_TRUE, "bigWin");
	ResMngrSinglton::GetInstance().LoadTexture("textures\\Cherry.png", GL_TRUE, "cherry");
	ResMngrSinglton::GetInstance().LoadTexture("textures\\Bar.png", GL_TRUE, "bar");
	ResMngrSinglton::GetInstance().LoadTexture("textures\\Orange.png", GL_TRUE, "orange");

	// ������� (�������� � ������������ ������ �������� ��������)
	m_MachineDisplay = std::make_shared<MachineDisplay>(glm::vec2(0, 0));
	m_SceneObjs.push_back(m_MachineDisplay);

	// ������� � ����������� FPS
	m_FPS = std::make_shared<FPS>(glm::vec2(10, 10));
	m_SceneObjs.push_back(m_FPS);

	// ������ �����
	m_StartBtn = std::make_shared<StartBtn>(glm::vec2(g_Width / 2, 80), m_MachineDisplay);
	m_SceneObjs.push_back(m_StartBtn);

	// ������ ����� ���������� ��������� ���������
	m_MachineDisplay->SubscribeBtn(m_StartBtn);
}

void SceneSingleton::MousBtnPressed(int button, int state, int x, int y)
{
	// ��������� ������ ����� ������ ���� ��� ������� UP
	if (state != GLUT_UP || button != GLUT_LEFT_BUTTON)
		return;

	// ������� � ������� ��������� ��������� �����
	auto btnCoordY = g_Height - y;

	m_StartBtn->TestAndClick(x, btnCoordY);
}

void SceneSingleton::Render()
{
	START_RENDER
		for (auto& obj : m_SceneObjs)
			obj->Render();
	END_RENDER
}

void SceneSingleton::Update()
{
	auto time = glutGet(GLUT_ELAPSED_TIME);
	auto deltaTime = time - m_LastTime;
	m_LastTime = time;

	for (auto& obj : m_SceneObjs)
		obj->Update(deltaTime);
}

SceneSingleton::SceneSingleton(): m_LastTime(0)
{
}