#include "SpriteObj.h"
#include "RenderUtils.h"

SpriteObj::SpriteObj(glm::vec2 pos, float scale, const std::string &sprite)
	: BaseObj(pos)
	, m_ScaleX(scale)
	, m_ScaleY(scale)
	, m_Srites(sprite)
{
}

SpriteObj::SpriteObj(glm::vec2 pos, float scaleX, float scaleY, const std::string &sprite)
	: BaseObj(pos)
	, m_ScaleX(scaleX)
	, m_ScaleY(scaleY)
	, m_Srites(sprite)
{
}

void SpriteObj::Render()
{
	RenderSprite(m_Srites, m_Pos.x, m_Pos.y, m_ScaleX, m_ScaleY);
}