#include "StartBtn.h"
#include "RenderUtils.h"
#include <cmath>
#include "Globals.h"

#define RED_COLOR 1
#define MIN_BRIGHTNESS 0.6f

StartBtn::StartBtn(glm::vec2 pos, std::weak_ptr<MachineDisplay> machineDisplay)
	: UpdatableObj(pos)
	, m_BtnName("START")
	, m_BackColor(RED_COLOR, MIN_BRIGHTNESS)
	, m_Time(0)
	, m_IsClicked(false)
	, m_Scale(50)
	, m_MachineDisplay(machineDisplay)
{
}

void StartBtn::Render()
{
	m_Pos.x = g_Width / 2;
	RenderTxt(m_BtnName.c_str(), m_Pos, m_Scale, m_BackColor);
}

void StartBtn::Update(int deltaTime)
{
	// ���� �������� ������ ���� ������ �������
	if (m_IsClicked)
		return;

	m_Time += deltaTime;
	if (m_Time > ONE_SEC)
		m_Time = 0;

	m_BackColor.y = cos(m_Time / ONE_SEC) - MIN_BRIGHTNESS;
}

void StartBtn::TestAndClick(int x, int y)
{
	if (m_IsClicked)
		return;

	auto leftBorder = m_Pos.x - m_Scale;
	auto rightBorder = m_Pos.x + m_Scale;

	auto topBorder = m_Pos.y - m_Scale;
	auto bottomBorder = m_Pos.y + m_Scale;

	// ���� ���� ��������� � �������� ������
	if ((leftBorder < x && x < rightBorder) && (topBorder < y && y < bottomBorder))
	{
		// ������ ����� �������� ������� ��������, ������
		// ����������� ����� ��������� ������ �����������
		m_MachineDisplay.lock()->StartRoll(4 * ONE_SEC);
		DisableBtn();
	}
}

void StartBtn::DisableBtn(bool disable)
{
	// ������ �������������� ����� �����, � 
	// ���������� ��������� ��� ��������� ���� ���������
	m_IsClicked = disable;
	m_BackColor.y = MIN_BRIGHTNESS;
}