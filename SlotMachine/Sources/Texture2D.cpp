#include <iostream>
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include "Texture2D.h"

Texture2D::Texture2D()
	: m_Width(0)
	, m_Height(0)
	, m_InternalFormat(GL_RGBA)
	, m_ImageFormat(GL_RGBA)
	, m_WrapS(GL_REPEAT)
	, m_WrapT(GL_REPEAT)
	, m_FilterMin(GL_LINEAR)
	, m_FilterMax(GL_LINEAR)
{
	glGenTextures(1, &m_ID);
}

void Texture2D::Generate(GLuint width, GLuint height, unsigned char* data)
{
	m_Width = width;
	m_Height = height;

	glBindTexture(GL_TEXTURE_2D, m_ID);
	glTexImage2D(GL_TEXTURE_2D, 0, m_InternalFormat, width, height, 0, m_ImageFormat, GL_UNSIGNED_BYTE, data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, m_WrapS);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, m_WrapT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, m_FilterMin);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, m_FilterMax);

	glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture2D::Bind() const
{
	glBindTexture(GL_TEXTURE_2D, m_ID);
}