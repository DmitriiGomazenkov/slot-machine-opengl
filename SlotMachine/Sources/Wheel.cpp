#include "Wheel.h"
#include "RenderUtils.h"
#include "MachineDisplay.h"

#define VISIBLE_SLOTS 4
#define SLOT_SHIFT 0.2f

Wheel::Wheel(glm::vec2 pos, MachineDisplay *machineDisplay)
	: UpdatableObj(pos)
	, m_Speed(0)
	, m_MachineDisplay(machineDisplay)
{
	m_CurYPos = pos.y;
	glm::vec2 curPos = m_Pos;

	auto AddSlot = [&](const std::string &sprite)
	{
		m_Slots.push_back(SpriteObj(curPos, 0.1f, sprite));
		curPos.y -= SLOT_SHIFT;
	};
	
	AddSlot("seven");
	AddSlot("waterM");
	AddSlot("plum");
	AddSlot("lemon");
	AddSlot("banana");
	AddSlot("bigWin");
	AddSlot("cherry");
	AddSlot("bar");
	AddSlot("orange");

	// ���������� ������� ������� ���������� 
	// ����� ������ ����� ��������
	auto firstInvis = std::next(m_Slots.begin(), VISIBLE_SLOTS);
	m_FirstInvisSlot = (*firstInvis).m_Pos.y;
}

Wheel::RollTimeMngr::RollTimeMngr()
	: m_RollStarted(false)
	, m_TargetRollTime(0)
	, m_CurrRollTime(0)
{
};

bool Wheel::RollTimeMngr::IsStopTime(int time)
{
	m_CurrRollTime += time;
	return m_CurrRollTime > m_TargetRollTime;
}

void Wheel::RollTimeMngr::SetTargetTime(int time)
{
	m_CurrRollTime = 0;
	m_RollStarted = true;
	m_TargetRollTime = time;
}

void Wheel::Update(int deltaTime)
{
	if (!m_RollTimeMngr.m_RollStarted)
		return;

	// ������������ ����� ��������
	auto isStopTime = m_RollTimeMngr.IsStopTime(deltaTime);

	// �������� ������ ������������ ����� ���������
	auto SlotsShift = [&]()
	{
		auto pos = m_CurYPos;

		for (auto& curSlot : m_Slots)
		{
			curSlot.m_Pos.y = pos;
			pos -= SLOT_SHIFT;
		}
	};

	// ������������� �������
	m_CurYPos -= m_Speed;
	SlotsShift();

	// ���� ��������� ������� ���� ����� �� ���� ���������, 
	// �� ������������ ��������� ������� ������ �� 1� �����
	auto end = std::next(m_Slots.begin(), VISIBLE_SLOTS - 1);
	if ((*end).m_Pos.y < m_FirstInvisSlot)
	{
		auto backSlot = m_Slots.back();
		m_Slots.begin()->m_Pos.y;

		backSlot.m_Pos.y = m_Slots.begin()->m_Pos.y + SLOT_SHIFT;
		m_CurYPos = backSlot.m_Pos.y;

		m_Slots.pop_back();
		m_Slots.push_front(backSlot);

		// ����������� �������, ����� ��� ����� ����� � ���� ���
		if (isStopTime)
		{
			m_RollTimeMngr.m_RollStarted = false;

			// ����������� �������
			m_CurYPos = m_Pos.y;
			SlotsShift();

			// ���������� �������, ��� �������� ��������
			m_MachineDisplay->WheelStoped();
		}
	}
}

void Wheel::Render()
{
	std::list<SpriteObj>::iterator it;
	auto end = std::next(m_Slots.begin(), VISIBLE_SLOTS);

	// ���������� ������ 4 �����
	for (it = m_Slots.begin(); it != end; ++it)
		(*it).Render();
}

void Wheel::StartRoll(float speed, int rollTime)
{
	m_Speed = speed;
	m_RollTimeMngr.SetTargetTime(rollTime);
}